This repository is NOT the official GroIMP release page.
If you want the latest releases please refer to the [release page](https://gitlab.com/grogra/groimp/-/releases)  or visit the official [website](grogra.de).

This repo only is used for storage of some old release files.